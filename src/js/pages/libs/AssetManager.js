// static class function
(function (global, factory) {
    typeof exports === "object" &&
        typeof module !== "undefined"
        ? factory(exports)
        : typeof define === "function" &&
            define.amd
            ? define(["exports"], factory)
            : (factory((global.AssetManager = {})))
}(this, (function (exports) {
    "use strict";

    var Event = {
        ON_LOAD_PROGRESS: 'onLoadProgress',
        ON_LOAD_COMPLETE: 'onLoadComplete',
        ON_LOADS_ERROR: 'onLoadError',
    }

    var cubeImages = [
        "right.jpg",
        "left.jpg",
        "top.jpg",
        "bottom.jpg",
        "front.jpg",
        "back.jpg",
    ];

    var listToLoad = [];
    var loaders = [];
    var loaded = 0;
    var total = 0;

    var list = new Map();

    // callback for load models complete:
    var _onComplete;

    var manager = new THREE.LoadingManager();

    // ALL ITEMS MANAGER:
    function start(onComplete) {
        _onComplete = onComplete;
        loaded = 0;
        total = listToLoad.length;

        if (total === 0) {
            if (_onComplete) _onComplete();
        }

        for (var i = 0; i < total; i++) {
            var key = listToLoad[i].key;
            var url = listToLoad[i].url;
            load(key, url, onLoaded);
        }
    }

    function onLoaded(key, loadedObject) {
        list.set(key, loadedObject);
        loaded++;

        total = listToLoad.length;

        if (loaded === total) {
            if (_onComplete) _onComplete();

            var evt = $.Event(Event.ON_LOAD_PROGRESS);
            evt.progress = 1;
            $(AssetManager).trigger(evt);

            var evt = $.Event(Event.ON_LOAD_COMPLETE);
            $(AssetManager).trigger(evt);

        } else {
            var progress = loaded / total;
            var evt = $.Event(Event.ON_LOAD_PROGRESS);
            evt.progress = progress;
            $(AssetManager).trigger(evt);
        }
    }

    // EACH ITEM:
    /**
     * this function add new object{key, url} to list of models to load
     * @param {*} key 
     * @param {*} url 
     */
    function add(key, url) {
        var itemToLoad = { key: key, url: url };
        listToLoad.push(itemToLoad);
    }

    /**
     * this function returns the loaded object by key from list of assets.
     * @param {*} key 
     */
    function get(key) {
        return list.get(key);
    }

    /**
     * this function addd the loaded object to list of assets with key.
     * @param {*} key 
     * @param {*} obj 
     */
    function set(key, obj) {
        obj.traverse(function (child) {
            if (child.isMesh) {
                child.castShadow = false;
                child.receiveShadow = false;
            }
        });
        list.set(key, obj);
    }

    function load(key, url, onLoad) {
        var loader;
        if (key.indexOf("cube_map") !== -1) {
            loader = new THREE.CubeTextureLoader(manager);
            loader.setPath(url);
            loader.load(cubeImages, onItemLoaded);
        } else {
            var fileExt = getExtension(url);
            switch (fileExt) {
                case "fbx":
                    loader = new THREE.FBXLoader(manager);
                    break;
                case "gltf":
                    laoder = new THREE.GLTFLoader(manager);
                    break;
                case "png":
                    loader = new THREE.TextureLoader(manager);
                    break;
                case "jpg":
                    loader = new THREE.TextureLoader(manager);
                    break;
                default:
                    loader = new THREE.FileLoader(manager);
                    break;
            }
            loader.load(url,
                onItemLoaded,
                onItemProgress,
                onItemError
            );
        }

        loaders.push(loader);


        function onItemLoaded(object) {
            var obj3d;
            if (typeof object.scene !== "undefined") {
                var holder = new THREE.Object3D();
                holder.add(object.scene);
                object.scene.scale.setScalar(100);
                obj3d = holder;
                if (typeof (object.animations) !== "undefined") {
                    obj3d.animations = object.animations;
                }
                if (typeof (object.assets) !== "undefined") {
                    obj3d.assets = object.assets;
                }
                if (typeof (object.parser) !== "undefined") {
                    obj3d.animations = object.parser;
                }

            } else {
                obj3d = object;
            }

            if (onLoad) onLoad(key, obj3d);

        };
        function onItemError(err) {
            console.log("[Assets] Error " + err.target.status + ": " + err.target.statusText);
        }
        function onItemProgress(xhr) {
            console.log((xhr.loaded / xhr.total * 100) + "%");
        }

        return loader;
    }

    function onLoadError(errItemUrl) {
        console.error("error on loading: ", errItemUrl);

    }

    function dispose() {
        loaders.forEach(function (loader) {
            loader = null;
        });
    }

    // UTILS:
    /**
 * This function will be use to get extension of url of a file
 * @param {*} url : url to the item
 * @returns {String} extension of a file
 */
    function getExtension(url) {
        var ext = url.substring(url.lastIndexOf('.') + 1, url.length) || url;
        return ext.toLowerCase();
    }

    exports.Event = Event;
    // exports.onLoadError = onLoadError;
    exports.add = add;
    exports.set = set;
    exports.get = get;
    exports.list = list;
    exports.start = start;
    exports.dispose = dispose;

    Object.defineProperty(exports, "__esModule", { value: true });

}))); 