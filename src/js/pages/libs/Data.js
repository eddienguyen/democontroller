var DIRECTION = {
    LEFT: "left",
    LIGHT: "right",
    FORWARD: "forward",
    // BACKWARD: "backward",
}

Object.defineProperties(DIRECTION, {
    random: {
        get: function () {
            var keys = Object.keys(DIRECTION)
            return DIRECTION[keys[keys.length * Math.random() << 0]];
        }
    }
});

var laneSize = 100;

var _forwardTexture = AssetManager.get("street_forward_texture"); _forwardTexture.anisotropy = 16;
_forwardTexture.wrapS = _forwardTexture.wrapT = THREE.repeatWrapping;
var _emptyTexture = AssetManager.get("street_empty"); _emptyTexture.anisotropy = 16;
_emptyTexture.wrapS = _emptyTexture.wrapT = THREE.repeatWrapping;
var _startTexture = AssetManager.get("street_cross_start_texture"); _startTexture.anisotropy = 16;
_startTexture.wrapS = _startTexture.wrapT = THREE.repeatWrapping;
var _endTexture = AssetManager.get("street_cross_end_texture"); _endTexture.anisotropy = 16;
_endTexture.wrapS = _endTexture.wrapT = THREE.repeatWrapping;

var GroundMaterial = {
    empty: new THREE.MeshBasicMaterial({ map: _emptyTexture, depthTest: true}),
    forward: new THREE.MeshBasicMaterial({ map: _forwardTexture, depthTest: true}),
    start: new THREE.MeshBasicMaterial({ map: _startTexture, depthTest: true}),
    end: new THREE.MeshBasicMaterial({ map: _endTexture, depthTest: true}),
}