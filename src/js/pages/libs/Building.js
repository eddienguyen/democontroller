// buildingArr.randomIndex() : returns random index of this building models
function Building() {
    var that = this;
    THREE.Object3D.call(that);
    that.name = "Building";
    var buildingArr = pMain.models.buildings;
    var defaultScale = 10;
    // private:

    // export public props:
    Object.defineProperties(that, {
        buildings: {
            get: function () {
                return buildingArr;
            }
        },
        defaultScale: {
            get: function () {
                return defaultScale;
            }
        }
    });

    // export public functions:
    Object.assign(Building.prototype, {
        init: function () {
            var that = this;
            that.randomBuilding();
        },
        createBuilding: function (randomizedIndex) {
            var that = this;
            var _clonedBuilding = AssetManager.get(buildingArr[randomizedIndex]).clone();
            _clonedBuilding.scale.setScalar(defaultScale);
            _clonedBuilding.receiveShadow = true;
            _clonedBuilding.castShadow = true;
            that.add(_clonedBuilding);
            console.log("_clonedBuilding: ", _clonedBuilding);
        },
        randomBuilding: function () {
            var that = this;
            that.createBuilding(buildingArr.randomIndex());
        }
    });

}

Building.prototype = Object.assign(Object.create(THREE.Object3D.prototype), {});