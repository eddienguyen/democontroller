// static class function
(function (global, factory) {
    typeof exports === "object" &&
        typeof module !== "undefined"
        ? factory(exports)
        : typeof define === "function" &&
            define.amd
            ? define(["exports"], factory)
            : (factory((global.MainEnvironment = {})))
}(this, (function (exports) {
    "use strict";
    var that = this;
    var scene, renderer, camera, container, clock, controls, stats;
    var lights = [];
    var clock = new THREE.Clock();
    function init() {

        clock = new THREE.Clock();
        container = pMain.container;
        scene = new THREE.Scene();
        renderer = new THREE.WebGLRenderer({
            antialias: true,
            alpha: true,
            logarithmicDepthBuffer: true
        });
        renderer.setSize(window.innerWidth, window.innerHeight);
        camera = new THREE.PerspectiveCamera(
            30, window.innerWidth / window.innerHeight, 1, 10000
        );
        camera.position.set(0, 100, 800);

        controls = new THREE.OrbitControls(camera);

        stats = new Stats();
        stats.showPanel(0);
        document.body.appendChild(stats.dom);

        createLights();
        container.append(renderer.domElement);
        animate();
    }

    function createLights() {
        scene.background = new THREE.Color(0xcccccc);
        var lightHolder = new THREE.Group({
            name: "light holder"
        });

        // general light
        var generalLight = new THREE.HemisphereLight(0xffffff, 0xffffff);
        generalLight.position.set(0, 500, 0);
        lights.push({
            name: "generalLight",
            type: "hemisphere",
            light: generalLight,
        });
        lightHolder.add(generalLight);

        // direct light
        var directLight = new THREE.DirectionalLight(0xffffff, 0.25);
        lightHolder.add(directLight);
        lights.push({
            name: "directLight",
            type: "directional",
            light: directLight
        });

        // spot light

        scene.add(lightHolder);
    }

    function animate() {
        var delta = clock.getDelta();
        stats.update();
        console.log(scene);
        window.requestAnimationFrame(animate);
        renderer.render(scene, camera);
        if (Player) Player.animate(delta);
        if (MapController) MapController.updateRoad();
    }

    exports.init = init;


    Object.defineProperty(exports, "__esModule", { value: true });
    Object.defineProperties(exports, {
        scene: {
            get: function () {
                return scene
            }
        },
        camera: {
            get: function () {
                return camera;
            }
        }

    })
})));