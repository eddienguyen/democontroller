function SideWalk() {
    var that = this;
    THREE.Object3D.call(that);

    var pavementGeom = new THREE.BoxGeometry(laneSize, 4, laneSize);
    var pavementMaterial = new THREE.MeshBasicMaterial({
        color: new THREE.Color(0xffffff)
    });
    var pavement = new THREE.Mesh(pavementGeom, pavementMaterial);
    pavement.position.x = -laneSize;
    pavement.position.y = 2;

    for (var i = 0; i < pavement.geometry.faces.length; i++) {
        pavement.geometry.faces[i].color.setHex(0x171616);
    }
    pavement.geometry.faces[4].color.setHex(0xaa9e9f);
    pavement.geometry.faces[5].color.setHex(0xaa9e9f);
    that.add(pavement);
    var building = new Building();
    building.position.set(-laneSize * 0.75, 4, 0);
    building.init();
    that.add(building);
    that.name = "sidewalk";

    // export public props:
    Object.defineProperties(that, {

    });
    // export public functions: 
    Object.assign(SideWalk.prototype, {
        init: function () {

        },
        initRotation: function (parentDirection) {
            var that = this;
            switch (parentDirection) {
                case DIRECTION.LEFT:
                    that.rotation.y = 0;
                    break;
                case DIRECTION.RIGHT:
                    that.rotation.y = -Math.PI;
                    break;
                default:
                    break;
            }
        }
    });

}

SideWalk.prototype = Object.assign(Object.create(THREE.Object3D.prototype), {});