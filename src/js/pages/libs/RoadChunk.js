/**
 * Each RoadChunk includes: 
 *  - ground with 3 lanes in the middle
 *  - 2 sidewalks: left && right
 *  - auto bot && rewards
 *  - trees
 */

function RoadChunk() {
    // Data.js: laneSize, direction, GroundMaterial
    var that = this;
    THREE.Object3D.call(that);

    that.name = "roadChunk";
    var groundDirection = DIRECTION.FORWARD;

    var groundGeom = new THREE.PlaneBufferGeometry(laneSize, laneSize, 2, 2);
    var groundMat = GroundMaterial.forward;
    var ground = new THREE.Mesh(groundGeom, groundMat);
    ground.name = "groundMesh";
    ground.receiveShadow = true;
    ground.rotation.x = -Math.PI / 2;

    that.add(ground);
    var leftSideWalk = new SideWalk();
    var rightSideWalk = new SideWalk();
    leftSideWalk.init();
    rightSideWalk.init();
    leftSideWalk.initRotation(DIRECTION.LEFT);
    rightSideWalk.initRotation(DIRECTION.RIGHT);
    that.add(leftSideWalk);
    that.add(rightSideWalk);

    // export props
    Object.defineProperties(that, {
        direction: {
            get: function () {
                return groundDirection;
            },
            set: function (newDirection) {
                groundDirection = newDirection;
            }
        },
        ground: {
            get: function () {
                return ground;
            },
        },
        leftSideWalk: {
            get: function () {
                return leftSideWalk;
            }
        },
        rightSideWalk: {
            get: function () {
                return rightSideWalk;
            }
        }
    });

    // export function
    Object.assign(RoadChunk.prototype, {
        init: function () {
            that.isCorner = false;
            // default: 
            that.ground.material = GroundMaterial.forward;
            // that.position.set(0,0,0);
            that.rotation.set(0,0,0);
        },
        updateMaterial: function () {
            that.ground.material.needsUpdate = true;
        },
        setupCorner: function (position) {
            // position: "start", "end", "center"
            switch (position) {
                case "start":
                    that.ground.material = GroundMaterial.start;
                    that.isCorner = true;

                    break;
                case "end":
                    that.ground.material = GroundMaterial.end;
                    that.isCorner = true;

                    break;
                case "center":
                    that.ground.material = GroundMaterial.empty;
                    break;
                default:
                    that.ground.material = GroundMaterial.forward;
                    break;
            }
        }
        // testFunc2: function () {
        //     return mesh;
        // }
    })
};

RoadChunk.prototype = Object.assign(Object.create(THREE.Object3D.prototype), {});