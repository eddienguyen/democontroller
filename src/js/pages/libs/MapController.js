; (function (global, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['exports'], factory);
    } else if (typeof exports === 'object' && typeof module !== 'undefined') {
        factory(exports);
    } else {
        (factory((global.MapController = {})))
    }
}(this, (function (exports) {
    'use strict';

    var that = this;
    var chunkHolder = new THREE.Object3D();
    var chunkList = [];
    var roadLength = 20;
    var currentDirection = DIRECTION.FORWARD;
    var nextDirection;

    function init() {
        chunkHolder.position.set(0, 0, 100);
        MainEnvironment.scene.add(chunkHolder);

        createRoad();
    }

    function createRoad() {
        for (var i = 0; i < roadLength; i++) {
            // switch (rdDirection) {
            //     case 1: // left
            //         direction = DIRECTION.LEFT
            //         break;
            //     case 2: // right
            //         direction = DIRECTION.RIGHT
            //         break;
            //     default: // forward
            //         direction = DIRECTION.FORWARD
            //         break;
            // }
            // var newChunk = createRoadChunk(laneSize, direction);

            var newChunk = new RoadChunk();
            // move this chunk's position to the end of chunkList:
            repositionChunk(newChunk);

            newChunk.position.z = -i * laneSize;
            newChunk.index = i;
            chunkList.push(newChunk);
            chunkHolder.add(newChunk);

        }
    }

    function createRoadChunk() {
        var texture = AssetManager.get("street_forward_texture");
        texture.repeat.set(1, 1);
        texture.wrapS = texture.wrapT = THREE.repeatWrapping;
        var geometry = new THREE.PlaneBufferGeometry(size, size, 2, 2);
        var material = new THREE.MeshBasicMaterial({
            map: texture,
            depthTest: true
        });
        var mesh = new THREE.Mesh(geometry, material);
        mesh.rotation.x = -Math.PI / 2;

        if (direction === DIRECTION.LEFT) {
            mesh.position.x -= size;
            mesh.rotation.z -= Math.PI / 2;
        } else if (direction === DIRECTION.RIGHT) {
            mesh.position.x += size;
            mesh.rotation.z += Math.PI / 2;
        }
        return mesh;
    }

    function updateRoad() {

        var frustum = new THREE.Frustum();
        frustum.setFromMatrix(new THREE.Matrix4().multiplyMatrices(MainEnvironment.camera.projectionMatrix, MainEnvironment.camera.matrixWorldInverse));

        for (var i = 0; i < chunkList.length; i++) {
            var chunkItem = chunkList[i];

            // console.log(i, chunk.position.z);
            // Your 3d point to check
            // var pos = new THREE.Vector3(x, y, z);
            if (!frustum.containsPoint(chunkItem.position)) {
                // Do something with the position...
                // console.log(i);
                if (i === 0) {
                    // move this chunk out of chunklist
                    var outOfScrChunk = chunkList.shift();
                    // reset index for the new shifted array:
                    resetChunkIndex();

                    // move this chunk's position to the end of chunkList:
                    repositionChunk(outOfScrChunk);
                    // push back this re-position chunk
                    chunkList.push(outOfScrChunk);
                    // TODO: reset chunkId too
                }
            }
        }
    }

    function repositionChunk(chunkItem) {
        chunkItem.index = chunkList.length;
        chunkItem.init();

        if (currentDirection === DIRECTION.FORWARD) {
            nextDirection = DIRECTION.random;
        } else if (currentDirection === DIRECTION.LEFT) {
            // chunkItem.setupCorner("")
        } else if (currentDirection === DIRECTION.RIGHT) {
            // chunkItem.setupCorner("");
        }

        // if this is not the first chunk when created:
        if (chunkList.length !== 0) {
            var lastChunk = chunkList[chunkList.length - 1];
            // var currentPointDirectionOfLast = lastChunk[]
            // console.log(chunkList.last);
            chunkItem.position.z = lastChunk.position.z + (-laneSize);

            currentDirection = nextDirection;
            chunkItem.direction = currentDirection;
        }
    }

    function resetChunkIndex() {
        for (var i = 0; i < chunkList.length; i++) {
            chunkList[i].index = i;
            // var item = chunkList[i];
            // item.index = i;
        }
    }

    exports.init = init;
    exports.updateRoad = updateRoad;

    Object.defineProperty(exports, '__esModule', { value: true });
    Object.defineProperties(exports, {
        chunkList: {
            get: function () {
                return chunkList;
            },
            // set: function (val) {
            //     return setVal(val)
            // }
        }
    })
})));