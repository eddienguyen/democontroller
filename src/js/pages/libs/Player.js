; (function (global, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['exports'], factory);
    } else if (typeof exports === 'object' && typeof module !== 'undefined') {
        factory(exports);
    } else {
        (factory((global.Player = {})))
    }
}(this, (function (exports) {
    'use strict';
    var that = this;
    // var pressHoldEvent = new CustomEvent("pressHold");
    var goal, raycaster, velocity;
    var mesh = AssetManager.get("bike");
    var holder = new THREE.Object3D();
    var temp = new THREE.Vector3();
    holder.name = "playerHolder";
    var isPowerOn = false;
    var maxSpeed = 20;

    function init() {
        mesh.rotation.y = -Math.PI / 2;
        holder.position.y = 2;
        holder.add(mesh);

        console.log(AppUtils);

        goal = new THREE.Object3D();
        goal.position.set(0, 20, 600);
        // goal.rotateY(-Math.PI / 2);
        holder.add(goal);
        velocity = new THREE.Vector3(0, 0, 0);
        raycaster = new THREE.Raycaster();
        MainEnvironment.scene.add(holder);

        connectListeners();
    }

    function connectListeners() {
        document.addEventListener("keydown", onKeyDown);
        document.addEventListener("keyup", onKeyUp);
    }

    function onKeyUp(event) {
        var rotateTarget = 0;
        holder.rotation.z -= (holder.rotation.z - rotateTarget);
        holder.rotation.y -= (holder.rotation.y - rotateTarget);
        velocity.x = 0;
    }

    function onKeyDown(event) {
        var keyCode = event.which || event.keyCode;

        switch (keyCode) {
            case 32:
                // space: start, break
                togglePower();
                break;
            case 65:
                // a: left
                if (!isPowerOn) return;
                turn(DIRECTION.LEFT);
                break;
            case 68:
                if (!isPowerOn) return;
                turn(DIRECTION.RIGHT);
                // d: right
                break;
            case 87:
                // w: 
                break;
            default:
                return;
        }
    }

    function togglePower() {
        if (isPowerOn) {
            isPowerOn = false;
        } else {
            isPowerOn = true;
            velocity.z = -1;
        }
    }

    function turn(direction) {
        // var quaternion = new THREE.Quaternion();
        // var currentAngle = mesh.rotation.z;
        var targetX;
        var acce;
        switch (direction) {
            case DIRECTION.LEFT:
                // currentAngle = ((Math.PI / 4) - currentAngle);
                // quaternion.setFromAxisAngle(new THREE.Vector3(0, 0, 1), currentAngle);

                // mesh.applyQuaternion(quaternion);

                // bike rotation z --
                var rotateTarget = Math.PI / 4;
                holder.rotation.z += (rotateTarget - holder.rotation.z) * 0.0256;
                holder.rotation.y += ((rotateTarget * 2) - holder.rotation.y) * 0.0128;

                // targetX = AppUtils.normalize(-5, -.75, .75, -5, 5);
                acce = new THREE.Vector3(-0.1, 0, 0);

                velocity.add(acce);

                break;
            case DIRECTION.RIGHT:
                var rotateTarget = -Math.PI / 4;
                holder.rotation.z += ((rotateTarget * 2) - holder.rotation.z) * 0.0256;
                holder.rotation.y += (rotateTarget - holder.rotation.y) * 0.0128;
                // currentAngle = ((-Math.PI / 4) - currentAngle);
                // quaternion.setFromAxisAngle(new THREE.Vector3(0, 0, 1), currentAngle);

                // mesh.applyQuaternion(quaternion);
                // bike rotation z ++
                acce = new THREE.Vector3(0.1, 0, 0);
                velocity.add(acce);
                break;
            default:
                break;
        }
    }

    function animate(delta) {

        if (!isPowerOn) return;

        velocity.z = Math.max(velocity.z * 60 * delta, -maxSpeed);

        holder.position.add(velocity);

        if (goal) {
            temp.setFromMatrixPosition(goal.matrixWorld);
            // let camera follow this mesh:
            MainEnvironment.camera.position.lerp(temp, 0.5);
            
            // MainEnvironment.camera.rotation.y += (mesh.rotation.y - MainEnvironment.camera.rotation.y) / 20;
            MainEnvironment.camera.lookAt(holder.position);

            // if (MainEnvironment.controls) {
            //     MainEnvironment.controls.target.copy(mesh.position);
            // }
        }

        raycastDown();
        checkOutOfGround();
    }

    function checkOutOfGround() {
        var groundObjects = MapController.chunkList.map(function (groundObj) {
            return groundObj.ground;
        });
        var intersectsWithGrounds = raycaster.intersectObjects(groundObjects, false);

        if (intersectsWithGrounds.length <= 0) {
            velocity.z -= Math.min(1, velocity.z);
            // togglePower();
        }
    }

    function raycastDown() {
        // raycaster
        // console.log(holder.position);
        // console.log(mesh.position);

        if (raycaster) {
            raycaster.set(holder.position, new THREE.Vector3(0, -1, 0));
        }
    }

    exports.init = init;
    exports.animate = animate;
    Object.defineProperty(exports, '__esModule', { value: true });
    Object.defineProperties(exports, {
        mesh: {
            get: function () {
                return mesh;
            },
            set: function (newMesh) {
                mesh = newMesh;
            }
        }
    })
})));