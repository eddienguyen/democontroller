var threePath = path_resource + "js/plugins/threejs-r108/";

var pMain = {
    container: document.getElementById("pMain"),
    preloadScripts: [
        threePath + "three.min.js",
        threePath + "libs/inflate.min.js",
        threePath + "libs/stats.min.js",

        // models loader:
        threePath + "loaders/FBXLoader.js",
        threePath + "loaders/GLTFLoader.js",

        path_resource + "js/plugins/TJS/TJS.js",

        path_resource + "js/pages/libs/AssetManager.js"
    ],

    importScripts: [
        threePath + "WebGL.js",
        threePath + "controls/OrbitControls.js",

        // public class, component class (map, player, main, etc,...)
        path_resource + "js/pages/libs/Data.js",
        path_resource + "js/pages/libs/AppUtils.js",

        // 
        path_resource + "js/pages/libs/MainEnvironment.js",
        path_resource + "js/pages/libs/MapController.js",
        path_resource + "js/pages/libs/Player.js",
        path_resource + "js/pages/libs/Building.js",
        path_resource + "js/pages/libs/SideWalk.js",
        path_resource + "js/pages/libs/RoadChunk.js"
    ],

    models: {
        buildings: [
            "Auto_service",
            "Bakery",
            "Bar",
            "Books_shop",
            "Chickenshop",
            "Clothing",
            "Coffeeshop",
            "Drugstore",
            "Fast_food",
            "Fruits_shop",
            "Gas_station",
            "House_01",
            "House_02",
            "House_03",
            "House_04",
            "Music_store",
            "Pizza",
            "Residential",
            "Restaurant",
            "Shoes_shop",
            "Sky_big",
            "Sky_small",
            "Super_Market",
        ],
        props: [
            "Bench_1",
            "Bench_2",
            "Bus_stop",
            "Dustbin",
            "Hydrant",
            "Street_light",
            "Traffic_cone",
            "Traffic_control_Barrier",
            "Traffic_signal_big",
            "Traffic_signal_small",
            "Windmill",
        ]
    },

    preload: function () {
        var that = this;

        // start loading model assets:
        AssetManager.add("background_not_road", path_resource + "images/textures/terrain/background_not_road.png");
        AssetManager.add("grasslight-big-nm", path_resource + "images/textures/terrain/grasslight-big-nm.jpg");
        AssetManager.add("grasslight-big", path_resource + "images/textures/terrain/grasslight-big.jpg");
        AssetManager.add("street_cross_end_texture", path_resource + "images/textures/terrain/street_cross_end_texture.png");
        AssetManager.add("street_cross_start_texture", path_resource + "images/textures/terrain/street_cross_start_texture.png");
        AssetManager.add("street_empty", path_resource + "images/textures/terrain/street_empty.png");
        AssetManager.add("street_forward_texture_256x256", path_resource + "images/textures/terrain/street_forward_texture_256x256.png");
        AssetManager.add("street_forward_texture", path_resource + "images/textures/terrain/street_forward_texture.png");

        // player stuffs:
        AssetManager.add("bike", path_resource + "assets/models/bike/bike.fbx");

        // building:
        var path = path_resource + "assets/models/City/Buildings/";
        for (var i = 0; i < that.models.buildings.length; i++) {
            var item = that.models.buildings[i];
            AssetManager.add(item, path + item + ".fbx");
        }

        // props:
        path = path_resource + "assets/models/City/Props/";
        for (var i = 0; i < that.models.props.length; i++) {
            var item = that.models.props[i];
            AssetManager.add(item, path + item + ".fbx");
        }

        AssetManager.start(function () {
            console.log("done loading assets", AssetManager.list);
            GLoader.loadScripts(that.importScripts, function () {
                that.start();
            });
        });
    },

    start: function () {
        PRELOADER.hide();
        // this.render();

        MainEnvironment.init();
        MapController.init();
        Player.init();

    },

    // render: function () {
    //     window.requestAnimationFrame(pMain.render);
    // },

    init: function () {
        var that = this;
        console.log('<pMain> => INIT!');

        $(window).on('resize', pMain.onResize);
        pMain.onResize();

        PRELOADER.show();

        GLoader.loadScripts(this.preloadScripts, function () {
            that.preload();
        });
    },

    onResize: function (e) {
        // do your fucking resizing
        console.log('Browser size: ' + window.innerWidth + 'x' + window.innerHeight);
    },
}