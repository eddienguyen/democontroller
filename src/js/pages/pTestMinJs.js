

var threePath = path_resource + 'js/plugins/threejs-r108/';

var pTestMinJs = {

    preloadScripts: [
        path_resource + "js/plugins/TJS/TJS.js",

        threePath + "three.min.js",
        threePath + 'libs/inflate.min.js', // for FBXLoader
        threePath + 'libs/stats.min.js', // for statistics

        // threePath + 'loaders/LoaderSupport.js',
        threePath + 'loaders/FBXLoader.js',
        threePath + 'loaders/GLTFLoader.js',

        path_resource + "js/pages/libs/AssetsManager.js",
    ],

    importScripts: [

    ],

    init: function () {

        PRELOADER.show();

        var _this = this;
        console.log("<pTestMinJs> => INIT!");

        $(window).on('resize', pTestMinJs.onResize);
        pTestMinJs.onResize();

        GLoader.loadScripts(
            _this.preloadScripts,
            function (result) {
                pTestMinJs.preLoad();
            })

        _this.addTestButton();
    },

    onResize: function (e) {
        // do your fucking resizing
        sw = $(window).width();
        sh = $(window).height();
    },

    preLoad: function () {
        ///
        PRELOADER.hide();

        // AssetsManager.add('auraglow_cube_map', path_resource + 'images/textures/auraglow_5/');
    },


    start: function () {


    },

    addTestButton: function () {
        var _this = this;

        if (!isTestingLocal) return;

        var textButton = "bundle preloadScripts"
            , importScriptsButton = "bundle importScripts"
            ;

        var _buttonWidth = 200
            , _buttonHeight = 60;
        var _buttonTest = `<ul id="js-buttonTestHolder" style="
            top: 60px;
            position: absolute;
            z-index: 999;">
            <li id="${"js-bundlePreloadScript"}"><button type="button" style="
            height: ${_buttonHeight}px;
            width: ${_buttonWidth}px;
            background-color: white;
            padding: 5px;
            margin: 5px;
            ">${textButton}</button></li>
            <li id="${"js-bundleImportScript"}"><button type="button" style="
            height: ${_buttonHeight}px;
            width: ${_buttonWidth}px;
            background-color: white;
            padding: 5px;
            margin: 5px;
            ">${importScriptsButton}</button></li>
            </ul>`;


        $(document.body).append(_buttonTest);

        $("#js-buttonTestHolder").css("z-index", "9999");
        $("#js-buttonTestHolder").css("pointer-even", "auto");

        $("#js-buttonTestHolder li").css("z-index", "9999");
        $("#js-buttonTestHolder li").css("pointer-even", "auto");

        $("#js-bundlePreloadScript").on("click", function () {
            TJS.BundleJs.writeArrayJsToFile(_this.preloadScripts, "preloadScripts");
        })
        $("#js-bundleImportScript").on("click", function () {
            TJS.BundleJs.writeArrayJsToFile(_this.importScripts, "importScripts");
        })


        function readJs(path, options) {
            options = typeof options == "undefined" ? {} : options;
            var onComplete = options.hasOwnProperty("onComplete") ? options['onComplete'] : null
                , onError = options.hasOwnProperty("onError") ? options['onError'] : null;

            $.ajax({
                method: "GET",
                url: path,
                complete: function (data) {
                    if (onComplete) onComplete(data);
                },
                error: function (err) {
                    if (onError) onError(err);
                }
            });
        }

        function writeToFile(string) {
            var _type = "text/plain;charset=utf-8";

            var blob = new Blob([], { type: _type });
            blob = new Blob([blob, " " + string], { type: _type });

            var url = URL.createObjectURL(blob);

            var a = document.createElement('a');
            a.download = "backup.txt";
            a.href = url;
            a.textContent = "Download backup.txt";
            a.click();
        }


        function writeArrayJsToFile(arrPath, fileName) {
            fileName = typeof fileName == "undefined" ? "fileName" : fileName;

            var currentIndex = 0;

            var _type = "text/plain;charset=utf-8";

            var blob = new Blob([], { type: _type });

            readJs(arrPath[currentIndex], {
                onComplete: onLoad,
                onError: onError
            });

            function onError(er) {
                console.log(er);
            }

            function onLoad(data) {
                currentIndex++;
                if (currentIndex <= arrPath.length) {
                    blob = new Blob([blob, " " + `//${arrPath[currentIndex - 1]} \n\n`], { type: _type });
                    blob = new Blob([blob, " " + data.responseText.toString()], { type: _type });

                    readJs(arrPath[currentIndex], {
                        onComplete: onLoad,
                        onError: onError
                    });

                } else {
                    onComplete();
                }
            }

            function onComplete() {

                var url = URL.createObjectURL(blob);

                var a = document.createElement('a');
                a.download = `${fileName}.txt`;
                a.href = url;
                a.textContent = "Download backup.txt";
                a.click();
            }
        }
    },


}